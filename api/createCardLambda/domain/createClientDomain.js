const { ageCalc } = require('../helper/genericsHelper');
const { ClientCreatedSchema } = require('../schema/event/createClientEvent');
const { ClientSchema } = require('../schema/input/clientSchemaInput');
const { DynamoDBClientService } = require('../service/dynamoDbClientService');
const { sendCreateClientSNSService } = require('../service/snsCreateClientService');

module.exports = async (commandPayload, commandMeta) => {
  console.log('Payload', commandPayload);
  new ClientSchema(commandPayload, commandMeta);
  const age = ageCalc(commandPayload.birthDay);
  if (age < 18) {
    return {
      statusCode: 400,
      body: JSON.stringify({
        message: 'El cliente debe ser mayor a 18 anios',
      }),
    };
  }
  if (age > 65) {
    return {
      statusCode: 400,
      body: JSON.stringify({
        message: 'El cliente debe ser menor a 65 anios',
      }),
    };
  }
  await DynamoDBClientService(commandPayload);
  await sendCreateClientSNSService(
    new ClientCreatedSchema(commandPayload, commandMeta)
  );
  return {
    statusCode: 200,
    body: 'Client created',
  };
};
