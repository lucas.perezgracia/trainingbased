const DYNAMODB = require('aws-sdk/clients/dynamodb');
const dynamodb = new DYNAMODB({ region: 'us-east-1' });

const tableData = process.env.DB_TABLE_NAME;

exports.handler = async (event) => {
  const eventBody = event.Records.map((elem) => JSON.parse(elem.body));
  for (const record of eventBody) {
    const client = JSON.parse(record.Message);
    const params = {
      ExpressionAttributeNames: {
        '#C': 'creditCard',
      },
      ExpressionAttributeValues: {
        ':c': {
          M: {
            number: {
              S: generateCCNumber(),
            },
            expiration: {
              S: generateExpirationDate(),
            },
            ccv: {
              S: generateCCV(),
            },
            type: {
              S: generateType(client.birthDay),
            },
          },
        },
      },
      Key: {
        dni: {
          S: client.dni,
        },
      },
      ReturnValues: 'ALL_NEW',
      TableName: tableData,
      UpdateExpression: 'SET #C = :c',
    };
    const clientupdated = await updateItem(params);
    if (!clientupdated) {
        return {
            statusCode: 400,
            body: "Could not update client with card"
        }
    }
  }
  const response = {
    statusCode: 200,
    body: "Client updated succesfully"
  };
  return response;
};

const controlAge = (data) => {
  const today = new Date()
  let birthDay = new Date(data)
  let controledAge = today.getFullYear() - birthDay.getFullYear()
  return controledAge;
}

const randomNumber = (minimum, maximum) => {
  return Math.round( Math.random() * (maximum - minimum) + minimum);
}

const generateCCNumber = () => {
  return `${randomNumber(0000,9999)}-${randomNumber(0000,9999)}-${randomNumber(0000,9999)}-${randomNumber(0000,9999)}`
};

const generateExpirationDate = () => {
  return `${randomNumber(01,12)}/${randomNumber(21,35)}`;
};

const generateCCV = () => {
  return `${randomNumber(000,999)}`
};

const generateType = (birthday) => {
  return controlAge(birthday) > 45 ? 'Gold' : 'Classic';
};

const updateItem = (params) => {
  try {
      const dbResult = dynamodb.updateItem(params).promise();
      return dbResult;
  } catch (error) {
      console.log('CATCH - putItem ', error);
      return false;
  }
}