function ageCalc(date) {
  let now = new Date();
  let birthDay = new Date(date);
  let age = now.getFullYear() - birthDay.getFullYear();
  return age;
}

module.exports = {
  ageCalc,
};
