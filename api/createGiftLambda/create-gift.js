const DYNAMODB = require('aws-sdk/clients/dynamodb');

const dynamodb = new DYNAMODB({region: 'us-east-1'});
const tableData = process.env.DB_TABLE_NAME;

exports.handler = async (event) => {
  
  const eventBody = event.Records.map((elem) => JSON.parse(elem.body));
    for (const record of eventBody) {
        const client = JSON.parse(record.Message);
        const params = {
            ExpressionAttributeNames: {
                '#G': 'gift',
            },
            ExpressionAttributeValues: {
                ':g': {
                    S: giftChoser(client.birthDay),
                },
            },
            Key: {
                dni: {
                    S: client.dni,
                },
            },
            ReturnValues: 'ALL_NEW',
            TableName: tableData,
            UpdateExpression: 'SET #G = :g',
        };
        const clientupdated = await updateItem(params);
        if (!clientupdated) {
            return {
                statusCode: 400,
                body: 'Could not update client with gift'
            }
        }
    }
    const response = {
        statusCode: 200,
        body: 'Client updated succesfully'
    };
    return response;
}

const giftChoser = (birthday) => {
    const month = parseInt(birthday.split('-')[1]) 
    const day = parseInt(birthday.split('-')[2]) 
    let gift
    if(month >= 1 && month <= 3 ){
      if (month === 3 && day >= 21){
        gift = 'Buzo'
      } else {
        gift = 'Remera'
      }
    }
    if(month >= 4 && month <= 6 ){
      if (month === 6 && day >= 21){
        gift = 'Sweater'
      } else {
        gift = 'Buzo'
      }
    }
    if(month >= 7 && month <= 9 ){
      if (month === 9 && day >= 21){
        gift = 'Camisa'
      } else {
        gift = 'Sweater'
      }
    }
    if(month >= 10 && month <= 12 ){
      if (month === 12 && day >= 21){
        gift = 'Remera'
      } else {
        gift = 'Camisa'
      }
    }
    return gift
}

const updateItem = (params) => {
    try {
        const dbResult = dynamodb.updateItem(params).promise();
        return dbResult;
    } catch (error) {
        console.log('CATCH - putItem ', error);
        return false;
    }
}