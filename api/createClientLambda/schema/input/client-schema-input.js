const { InputValidation } = require('ebased/schema/inputValidation');

class ClientSchema extends InputValidation {
  constructor(payload, meta) {
    super({
      source: meta.source,
      payload,
      type: 'CLIENT.LAMBDA',
      specversion: '1.0.0',
      schema: {
        strict: false,
        dni: { type: String, required: true },
        firstName: { type: String, required: true },
        lastName: { type: String, required: true },
        birthDay: { type: String, required: true },
      },
    });
  }
}

module.exports = {
  ClientSchema,
};
