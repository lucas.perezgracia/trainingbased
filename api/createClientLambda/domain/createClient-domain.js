const { ageCalc } = require('../helper/generics-helper');
const { ClientCreatedSchema } = require('../schema/event/createClient-schema-event');
const { ClientSchema } = require('../schema/input/client-schema-input');
const { DynamoDBClientService } = require('../service/dynamoDbClient-service');
const { sendCreateClientSNSService } = require('../service/snsCreateClient-service');

module.exports = async (commandPayload, commandMeta) => {
  console.log('Payload', commandPayload);
  new ClientSchema(commandPayload, commandMeta);
  const age = ageCalc(commandPayload.birthDay);
  if (age < 18) {
    return {
      statusCode: 400,
      body: JSON.stringify({
        message: 'El cliente debe ser mayor a 18 anios',
      }),
    };
  }
  if (age > 65) {
    return {
      statusCode: 400,
      body: JSON.stringify({
        message: 'El cliente debe ser menor a 65 anios',
      }),
    };
  }
  await DynamoDBClientService(commandPayload);
  await sendCreateClientSNSService(
    new ClientCreatedSchema(commandPayload, commandMeta)
  );
  return {
    statusCode: 200,
    body: JSON.stringify({message: 'Client created'}),
  };
};
