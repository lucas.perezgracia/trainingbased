const dynamodb = require('ebased/service/storage/dynamo');

const DynamoDBClientService = async (payload) => {
  console.log('DynamoDBClientService   ', payload)
  await dynamodb.putItem({
    TableName: process.env.DB_TABLE_NAME,
    Item: payload,
  });
};

module.exports = { DynamoDBClientService };
