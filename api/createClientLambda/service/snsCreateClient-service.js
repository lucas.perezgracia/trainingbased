const sns = require("ebased/service/downstream/sns");

const sendCreateClientSNSService = async (clientCreateEvent) => {
  const { eventPayload, eventMeta } = clientCreateEvent.get();

  await sns.publish(
    {
      TopicArn: process.env.SNS_ARN,
      Message: eventPayload,
    },
    eventMeta
  );
};

module.exports = { sendCreateClientSNSService };
